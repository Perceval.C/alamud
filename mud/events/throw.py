# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class ThrowEvent(Event2):
    NAME = "throw"

    def perform(self):
        self.inform("throw")


class ThrowWithEvent(Event3):
    NAME = "throw-with"

    def perform(self):
        self.inform("throw-with")

